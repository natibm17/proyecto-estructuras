/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
using namespace std;

class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int tag2;
  int associativity;
  int mru;
  int lru = 0;
  enum miss_hit_status expected_miss_hit;
  bool loadstore;
  bool debug;
  struct operation_result result{};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  
  associativity = 1<<(rand()%4);
  mru = associativity - 1;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity];

  /* Check for a miss */
  DEBUG(debug_on, Checking miss operation);
  tag2 = tag;
  for (int i = 0; i < 2; i++){
    /* Fill cache line */
    for (int i = 0; i < associativity; i++) {
      cache_line[i].valid = true;
      
    //   while(tag == tag2){
		// 	 tag2 = rand()%4096;
		// }

      tag2++;
      cache_line[i].tag = tag2;  
          
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = mru;
      for (int k = 0; k < associativity; k++){
        if (cache_line[k].valid){         
          if (cache_line[k].rp_value != lru){
            cache_line[k].rp_value = cache_line[k].rp_value - 1;            
          }
        }
      }
      // cout<<"asoc: "+to_string(associativity)<<endl;
            
      // cout<<to_string(cache_line[i].tag)+" "+to_string(cache_line[i].rp_value)<<endl;
      // while (cache_line[i].tag == tag) {
      //   cout<<"NOT"<<endl;
      //   cache_line[i].tag = rand()%4096;
      // }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    // for (int i = 0; i < associativity; i++){
    //   cout<<"TAG cache: "+to_string(cache_line[i].tag)+" TAG: "+to_string(tag) <<endl;
    // }
    status = lru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_line,
                               &result,
                               bool(debug_on));
    
    
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
    // cout<<"193 no"<<endl;
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
    // cout<<"213 no"<<endl;
  }
}


/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                               tag,
                               associativity,
                               loadstore,
                               cache_line,
                               &result,
                               bool(debug_on));

    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  int status;
  int i;
  int idx;
  int tag;
  int policy;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};

  /* Choose a random policy */
  policy = rand()%3;

  /* Choose a random associativity */
  associativity =  1 << (rand()%4);

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;

  struct entry cache_line[associativity];
  /* Force a hit */

  DEBUG(debug_on,Checking promotion);

    /* Fill cache entry */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      while (cache_line[i].tag  == tag){
        cache_line[i].tag = rand()%4096;
      }      
      // cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = false;


      switch (policy)
      {
        case LRU:
          cache_line[i].rp_value = 0;
          break;
        case NRU:
          cache_line[i].rp_value = 1;
          break;
        case RRIP:
          if (associativity <= 2)
          {
            cache_line[i].rp_value = 1;
          }
          else
          {
            cache_line[i].rp_value = 3;
          }
          break;
      }
    }
      /* Force a hit on A */
    int tag_bA = cache_line[0].tag;

    switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag_bA,
                                associativity,
                                loadstore,
                                cache_line,
                                &result,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag_bA,
                                   associativity,
                                   loadstore,
                                   cache_line,
                                   &result,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag_bA,
                                         associativity,
                                         loadstore,
                                         cache_line,
                                         &result,
                                         bool(debug_on));
        break;
    }



    /*Check rp_value of block A */
    uint8_t rp_value_bA = cache_line[0].rp_value;
    int rp_expected;
    switch (policy)
    {
      case LRU:
        rp_expected = associativity - 1;
        EXPECT_EQ(rp_value_bA, rp_expected);
        break;
      case NRU:
        rp_expected = 0;
        EXPECT_EQ(rp_value_bA, rp_expected);
        break;
      case RRIP:
        rp_expected = 0;
        EXPECT_EQ(rp_value_bA, rp_expected);
        break;
    }

    /* Keep inserting new blocks until A is evicted*/
    if (policy == RRIP && associativity > 2)
    {
      while (result.evicted_address != tag_bA)
      {
        tag = rand()%4096;
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line,
                                         &result,
                                         bool(debug_on));
      }
    }
    else
    {
      for(int i = 0; i < associativity; i++)
      {
        tag = rand()%4096;

        switch (policy)
        {
          case LRU:
            lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
          break;
          case NRU:
            nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line,
                                       &result,
                                       bool(debug_on));

          break;
          case RRIP:
            srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line,
                                             &result,
                                             bool(debug_on));
          break;
        }
      }
    }

    /* Check eviction of block A happen after N new blocks were inserted */
    EXPECT_EQ(result.evicted_address, tag_bA);
}

/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status;
  int i;
  int idx;
  int tag;
  int policy;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};
  struct operation_result resultA = {};

  /* Choose a random policy */
  policy = rand()%3;


  /* Choose a random associativity */
  associativity = 1 << (rand()%4);

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line_A[associativity];
  /* Force a write hit for block A */
  loadstore = 1;
  DEBUG(debug_on,Checking writeback);

    /* Fill cache entry */
    for ( i =  0; i < associativity; i++) {
      cache_line_A[i].valid = true;
      cache_line_A[i].tag = rand()%4096;
      cache_line_A[i].dirty = false;
      while (cache_line_A[i].tag == tag) {
        cache_line_A[i].tag = rand()%4096;
      }
      switch (policy)
      {
        case LRU:
          cache_line_A[i].rp_value = 0;
          break;
        case NRU:
          cache_line_A[i].rp_value = 1;
          break;
        case RRIP:
          if (associativity <= 2)
          {
            cache_line_A[i].rp_value = 0;
          }
          else
          {
            cache_line_A[i].rp_value = 2;
          }
          break;
      }
    }
    /* Insert a new block A */
    int tag_bA = tag;
    switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
        break;
    }

   /*Force a write hit on A */
   switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
        break;
    }

    /*Force a read hit on A */
    loadstore = 0;
    switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_A,
                                &resultA,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_A,
                                   &resultA,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_A,
                                         &resultA,
                                         bool(debug_on));
        break;
    }
    /*Create random block B */
  idx = rand()%1024;
  tag = rand()%4096;
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line_B[associativity];
  /* Force a read hit for block B */
  loadstore = 0;

  DEBUG(debug_on,Checking writeback);

    /* Fill cache entry */
    for ( i =  0; i < associativity; i++) {
      cache_line_B[i].valid = true;
      cache_line_B[i].tag = rand()%4096;
      cache_line_B[i].dirty = false;
      while (cache_line_B[i].tag == tag) {
        cache_line_B[i].tag = rand()%4096;
      }
      switch (policy)
      {
        case LRU:
          cache_line_B[i].rp_value = 0;
          break;
        case NRU:
          cache_line_B[i].rp_value = 1;
          break;
        case RRIP:
          if (associativity <= 2)
          {
            cache_line_B[i].rp_value = 0;
          }
          else
          {
            cache_line_B[i].rp_value = 2;
          }
          break;
      }
    }
    /* Insert a new block B */
    int tag_bB = tag;
    switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_B,
                                &result,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_B,
                                   &result,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_B,
                                         &result,
                                         bool(debug_on));
        break;
    }
    /* Force a read hit on B */
    switch (policy)
    {
      case LRU:
        lru_replacement_policy (idx,
                                tag,
                                associativity,
                                loadstore,
                                cache_line_B,
                                &result,
                                bool(debug_on));
        break;
      case NRU:
        nru_replacement_policy(idx,
                                   tag,
                                   associativity,
                                   loadstore,
                                   cache_line_B,
                                   &result,
                                   bool(debug_on));
        break;
      case RRIP:
        srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line_B,
                                         &result,
                                         bool(debug_on));
        break;
    }

 /* Keep inserting new blocks until B is evicted*/

      while(result.evicted_address != tag_bB)
      {
        tag = rand()%4096;
        switch (policy)
        {
          case LRU:
            lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line_B,
                                    &result,
                                    bool(debug_on));
          break;
          case NRU:
            nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line_B,
                                       &result,
                                       bool(debug_on));
          break;
          case RRIP:
            srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line_B,
                                             &result,
                                             bool(debug_on));

          break;
        }
      }
    //}

  DEBUG(debug_on,Checking dirty bit);


    EXPECT_FALSE(result.dirty_eviction);
    EXPECT_EQ(result.evicted_address, tag_bB);

    /* Keep inserting new blocks until A is evicted*/

      while(resultA.evicted_address != tag_bA)
      {
        tag = rand()%4096;
        switch (policy)
        {
          case LRU:
            lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line_A,
                                    &resultA,
                                    bool(debug_on));
          break;
          case NRU:
            nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line_A,
                                       &resultA,
                                       bool(debug_on));
          break;
          case RRIP:
            srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line_A,
                                             &resultA,
                                             bool(debug_on));

          break;
        }
      }
    //}



    /*Check dirty bit for block A */
    //int dirtyA;
    EXPECT_TRUE(resultA.dirty_eviction);
     EXPECT_EQ(resultA.evicted_address, tag_bA);
  //   for ( i =  0; i < associativity; i++) {
  //     if(cache_line_A[i].tag == tag) {
  //       dirtyA = cache_line_A[i].dirty;
  //   break;
  //   }
  // }
  //   EXPECT_TRUE(dirtyA);



}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){//check

  int idx;
  int tag;
  int associativity = 1;//just necesary for cache line definition
  int policy;
  bool loadstore;
  struct entry cache_line[associativity];

  struct operation_result result = {};
  bool debug;
  int expected_param_result = PARAM;
  int parameter_result;

  /*Choose a random policy*/
  policy = rand()%3;//Check


  /*Choose invalid parameters for idx, tag and asociativy*/
  idx = -1;
  tag = -4;
  associativity = 12;


  switch (policy){
  case LRU:
    parameter_result = lru_replacement_policy (idx,
                                    tag,
                                    associativity,
                                    loadstore,
                                    cache_line,
                                    &result,
                                    bool(debug_on));
    EXPECT_EQ(expected_param_result, parameter_result);
    break;
  case NRU:
    parameter_result = nru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line,
                                       &result,
                                       bool(debug_on));
    EXPECT_EQ(expected_param_result, parameter_result);
    break;
  case RRIP:
   parameter_result = srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             loadstore,
                                             cache_line,
                                             &result,
                                             bool(debug_on));
    EXPECT_EQ(expected_param_result, parameter_result);
    break;

  default:
    break;
  }


}
