#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/L1cache.h"
#include "../include/debug_utilities.h"
using namespace std;

/* Print Cache Configuration */
void print_config(cache_params cache_params)
{
  cout << "---------------------------------------\n" ;
  cout << "Cache Parameters:\n" ;
  cout << "---------------------------------------\n" ;
  cout << "Cache Size (KB):           "<< cache_params.size << "\n";
  cout << "Cache Associativity:       "<< cache_params.asociativity << "\n";
  cout <<"Cache Block Size (bytes):  "<< cache_params.block_size << "\n";
  cout << "---------------------------------------\n" ;

}

/* Print Statistics */
void print_statistics(int miss_store, int miss_load, int hit_store, int hit_load, int dirty_eviction, int ic_t)
{
  int cpu_time;
  double read_miss_rate, miss_rate, amat = 0.0;

  /* Calculate rates and times*/

  int total_misses = miss_load + miss_store;
  int total_hits = hit_load + hit_store;
  miss_rate = (float)total_misses/(float)(total_misses + total_hits);
  cpu_time = ((float)ic_t/(float)(total_hits + total_misses)) * (1+ miss_rate*20);
  read_miss_rate = (float)miss_load / (float)(total_misses + total_hits);
  amat = 1.0 + (float)(miss_rate * 20);

  cout << "Simulation Results:\n" ;
  cout << "---------------------------------------\n" ;
  cout << "CPU time (cycles):        " << cpu_time << "\n";
  cout <<  "AMAT (cycles):            "<< amat << "\n";
  cout <<  "Overall miss rate:        "<< miss_rate << "\n";
  cout <<  "Read miss rate:           "<< read_miss_rate << "\n";
  cout <<  "Dirty evictions:          "<< dirty_eviction << "\n";
  cout <<  "Load misses:              "<< miss_load << "\n";
  cout <<  "Store misses:             "<< miss_store << "\n";
  cout <<  "Total misses:             "<< total_misses << "\n";
  cout <<  "Load hits:                "<< hit_load << "\n";
  cout <<  "Store hits:               "<< hit_store << "\n";
  cout <<  "Total hits:               "<< total_hits << "\n";
  cout << "---------------------------------------\n" ;

}

int main(int argc, char * argv []) {
  /* Parse argruments */
  cache_params cache_params;
  uint16_t  rp;
  for (size_t i = 0; i < argc; i++)
    {
    /* Cache size */
    if (strcmp(argv[i], "-t") == 0)
    {
      cache_params.size = atoi(argv[i + 1]);
    }
    /* Line size (bytes) */
    else if (strcmp(argv[i], "-l") == 0)
    {
      cache_params.block_size = atoi(argv[i + 1]);
    }
    /* Associativity */
    else if (strcmp(argv[i], "-a") == 0)
    {
      cache_params.asociativity = atoi(argv[i + 1]);
    }
    /* Replacement policy */
    else if (strcmp(argv[i], "-rp") == 0)
    {
      rp = atoi(argv[i + 1]);
    }
  }

  cache_field_size field_size;
  field_size_get(cache_params, &field_size);
  operation_result operation_result;

  char data[100];
  char address_hex[9];
  int loadstore;
  long address;
  int idx, tag, ic, ic_t = 0;
  bool debug = false;
  int miss_store, miss_load, hit_store, hit_load, dirty_eviction = 0;
  int result_check;

  /* Create cache */
  entry ** cache = create_cache(rp, cache_params.asociativity, field_size.idx);
  /* Get trace's lines and start your simulation */
  while (fgets(data, 100, stdin) != NULL)
  {
    /* Parse line */
    if (data[0] == 35) // # = 35 in ASCII
    {
      /* Get loadstore and address */
      sscanf(data, "%*s %d %s %d", &loadstore, address_hex, &ic);
      ic_t = ic_t + ic;
      /* Address from hex to decimal */
      stringstream ss;
      ss << hex << address_hex;
      ss >> address;
    }
    else
    {
      break;
    }

    address_tag_idx_get(address, field_size, &idx, &tag);

    switch(rp)
    {
      case LRU:
        result_check = lru_replacement_policy(idx, tag, cache_params.asociativity, loadstore, cache[idx], &operation_result, debug);
        break;
      case NRU:
        result_check = nru_replacement_policy(idx, tag, cache_params.asociativity, loadstore, cache[idx], &operation_result, debug);
        break;
      case RRIP:
        result_check = srrip_replacement_policy(idx, tag, cache_params.asociativity, loadstore, cache[idx], &operation_result, debug);
        break;
    }
    if (result_check==PARAM){
      cout<<"Invalid parameters inserted"<<endl;
      return ERROR;
    }
    
    if(operation_result.dirty_eviction)
    {
      dirty_eviction++;
    }

    if(operation_result.miss_hit == MISS_LOAD)
    {
      miss_load++;
    }
    else if(operation_result.miss_hit == MISS_STORE)
    {
      miss_store++;
    }
    else if(operation_result.miss_hit == HIT_LOAD)
    {
      hit_load++;
    }
    else if(operation_result.miss_hit == HIT_STORE)
    {
      hit_store++;
    }
  }

/* Print cache configuration */
  print_config(cache_params);

/* Print Statistics */
  print_statistics(miss_store, miss_load, hit_store, hit_load, dirty_eviction, ic_t);

  return 0;
}
