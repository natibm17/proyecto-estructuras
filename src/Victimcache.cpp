/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
#define ASSOC 16
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	bool loadstore,
       	                    	entry* l1_cache_blocks,
       	                  	 	entry* vc_cache_blocks,
        	                 	operation_result* l1_result,
              	              	operation_result* vc_result,
                	         	bool debug){
  //checks parameters
  int param_check_l2 = params_check(l1_vc_info->l1_idx, l1_vc_info->l1_tag, l1_vc_info->l1_assoc);
	if (param_check_l2!=OK){
		return PARAM;
	}

	bool l1_full = true;
	bool l1_hit_flag = false;
	int lru_val = 0;
	int mru_val =  l1_vc_info->l1_assoc - 1;
	int l1_hit_way = -1;
	int l1_free_way = -1;
	int l1_victim = -1;


	for (int way = 0; way < l1_vc_info->l1_assoc; way++){
	//Hit
		if (l1_cache_blocks[way].valid && l1_cache_blocks[way].tag == l1_vc_info->l1_tag){
			l1_hit_flag = true;
			l1_hit_way = way;
		}
		//Space check
		if (l1_cache_blocks[way].valid == false) {
			l1_full = false;
			l1_free_way = way;

		}
	}
	//check hit status
	if (l1_hit_flag){
		//Store
		if (loadstore){
			l1_result->miss_hit = HIT_STORE;
		//Load
		}else{
			l1_result->miss_hit = HIT_LOAD;
		}
		l1_result->evicted_address = 0;
		l1_result->dirty_eviction = false;

		for (int i = 0; i < l1_vc_info->l1_assoc; i++){
			if (l1_cache_blocks[i].rp_value>l1_cache_blocks[l1_hit_way].rp_value){
				if (l1_cache_blocks[i].rp_value!=lru_val){
					l1_cache_blocks[i].rp_value = l1_cache_blocks[i].rp_value - 1;//lru substraction
				}
			}
		}
		l1_cache_blocks[l1_hit_way].rp_value = mru_val;//hit gets max value
		if(loadstore){
			l1_cache_blocks[l1_hit_way].dirty = true;
		}
	//Miss
	}else{

		//Store
		if (loadstore){
			l1_result->miss_hit = MISS_STORE;
		//Load
		}else{
			l1_result->miss_hit = MISS_LOAD;
		}
		//Free Space
		if (l1_full == false){
			l1_result->dirty_eviction = false;
			l1_result->evicted_address = 0;
			for (int i = 0; i < l1_vc_info->l1_assoc; i++){
				if (l1_cache_blocks[i].valid){
					if (l1_cache_blocks[i].rp_value != lru_val){
						l1_cache_blocks[i].rp_value = l1_cache_blocks[i].rp_value - 1;
					}
				}
			}
			//check dirty
			if (loadstore){
				l1_cache_blocks[l1_free_way].dirty = true;
			}else{
				l1_cache_blocks[l1_free_way].dirty = false;
			}
			l1_cache_blocks[l1_free_way].rp_value = mru_val;//insertion gets max value
			l1_cache_blocks[l1_free_way].tag = l1_vc_info->l1_tag;
			l1_cache_blocks[l1_free_way].valid = true;

		//l1_full cache
		}else{

			for (int i = 0; i < l1_vc_info->l1_assoc; i++){
				if (l1_cache_blocks[i].rp_value == lru_val){
					l1_victim = i;//l1_victim way
				}else{
					l1_cache_blocks[i].rp_value = l1_cache_blocks[i].rp_value -1;//lru substraccion
				}
			}
			l1_result->evicted_address = l1_cache_blocks[l1_victim].tag;
			if (l1_cache_blocks[l1_victim].dirty){//checks dirty
				l1_result->dirty_eviction = true;
			}else{
				l1_result->dirty_eviction = false;
			}
			//check dirty
			if (loadstore){
				l1_cache_blocks[l1_victim].dirty = true;
			}else{
				l1_cache_blocks[l1_victim].dirty = false;
			}
			l1_cache_blocks[l1_victim].rp_value = mru_val;//insertion gets max value
			l1_cache_blocks[l1_victim].valid = true;
			l1_cache_blocks[l1_victim].tag = l1_vc_info->l1_tag;


		}
	}
	// l1 done vc comes now
	bool vc_full = true;
	bool vc_hit_flag = false;
	int fifo_first = 0;
	int fifo_last = l1_vc_info->vc_assoc - 1;
	int vc_hit_way = -1;
	int vc_free_way = -1;
	int vc_victim = -1;

	// checks incoming data is valid
	if (l1_cache_blocks[l1_victim].valid == true && l1_result->evicted_address!=0){
		//check miss in l1
		if(l1_result->miss_hit==MISS_LOAD || l1_result->miss_hit==MISS_STORE){
			for (int way = 0; way < l1_vc_info->vc_assoc; way++){
				if (vc_cache_blocks[way].valid && vc_cache_blocks[way].tag == l1_vc_info->l1_tag){
					vc_hit_flag = true;
					vc_hit_way = way;
				}
				//Space check
				if (vc_cache_blocks[way].valid == false) {
					vc_full = false;
					vc_free_way = way;
				}
			}
			// VC hit
			if (vc_hit_flag){
				//Store
				if (loadstore){
					vc_result->miss_hit = HIT_STORE;
				//Load
				}else{
					vc_result->miss_hit = HIT_LOAD;
				}
				if (vc_cache_blocks[vc_hit_way].dirty){
					vc_result->dirty_eviction = true;
				}else{
					vc_result->dirty_eviction = false;
				}

				vc_cache_blocks[vc_hit_way].valid = true;//vc cache only enter valid data
				vc_result->evicted_address = vc_cache_blocks[vc_hit_way].tag;
				// sends data to l1
				l1_cache_blocks[l1_victim].dirty = vc_cache_blocks[vc_hit_way].dirty;
				l1_cache_blocks[l1_victim].tag = vc_result->evicted_address;

				//saves incoming data in vc
				vc_cache_blocks[vc_hit_way].tag = l1_result->evicted_address;
				vc_cache_blocks[vc_hit_way].dirty = l1_result->dirty_eviction;
				for (int i = 0; i < l1_vc_info->vc_assoc; i++){
					if (i!=vc_hit_way){
						vc_cache_blocks[i].rp_value = vc_cache_blocks[i].rp_value + 1;//updates fifo values
					}else{
						vc_cache_blocks[i].rp_value = fifo_first;//entry value
					}
				}
			//VC miss
			}else{
				//Store
				if (loadstore){
					vc_result->miss_hit = MISS_STORE;
				}else{
					vc_result->miss_hit = MISS_LOAD;
				}
				// vc not full
				if (vc_full == false){
					vc_cache_blocks[vc_free_way].tag = l1_result->evicted_address;
					vc_cache_blocks[vc_free_way].dirty = l1_result->dirty_eviction;
					vc_cache_blocks[vc_free_way].valid = true;//already checked, vc dont admit invalid data
					for (int i = 0; i < l1_vc_info->vc_assoc; i++){
						if (i!=vc_free_way){
							vc_cache_blocks[i].rp_value = vc_cache_blocks[i].rp_value + 1;//adds rp
						}else{
							vc_cache_blocks[i].rp_value = fifo_first;//entry value
						}
					}
				// vc full
				}else{
					for (int i = 0; i < l1_vc_info->vc_assoc; i++){
						if (vc_cache_blocks[i].rp_value == fifo_last){
							vc_result->evicted_address = vc_cache_blocks[i].tag;
							vc_result->dirty_eviction = vc_cache_blocks[i].dirty;
							vc_cache_blocks[i].tag = l1_result->evicted_address;
							vc_cache_blocks[i].valid = true;
							for (int k = 0; k < l1_vc_info->vc_assoc; k++){
								if (i!=k){
									vc_cache_blocks[k].rp_value = vc_cache_blocks[k].rp_value + 1;//adds rp
								}else{
									vc_cache_blocks[k].rp_value = fifo_first;//victim value
								}
							}
							break;
						}
					}
				}
			}
		}
	}
	return OK;
}
