/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;



int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				operation_result* operation_result_cache_obl,
                                bool debug=false)
{
int param_check = params_check(idx, tag, associativity);
        if (param_check!=OK){
        return PARAM;
}

    bool hit_flag= false; 
    bool hit_obl = false; 
    bool prefetch = false; 
    
   //loadstore true es store 
 
    for (int way =0; way<associativity; way++){
        /*Hit Block B */  
        if (cache_block[way].tag == tag && cache_block[way].valid)
        {        
                hit_flag= true; 
                lru_replacement_policy(idx, tag, associativity, loadstore, cache_block, operation_result_cache_block, debug);
                if (loadstore){
                        operation_result_cache_block -> miss_hit = HIT_STORE;
                } else {
                        operation_result_cache_block -> miss_hit = HIT_LOAD; 
                }
                
                /*Check obl tag to verify prefetch */ 
                if (cache_block[way].obl_tag ==1){
                        
                        prefetch = true; 
                        /*Update obl tag to 0*/
                        cache_block[way].obl_tag =0; 
                        
                } else {
                        return OK;
                }
                //cache_block_obl[way].dirty =false;
                /*If obl tag is 0, no action is needed */ 
        } else{
                /* Miss Block B */  
                lru_replacement_policy(idx, tag, associativity, loadstore, cache_block, operation_result_cache_block, debug);
               if (loadstore){
                        operation_result_cache_block -> miss_hit = MISS_STORE;
                } else {
                        operation_result_cache_block -> miss_hit = MISS_LOAD; 
                }
                /* Check if block B+1 is in cache */ 
                cache_block[way].obl_tag = 0; 
                prefetch = true; 
                cache_block_obl[way].dirty =false;
        }
}

        /*Prefetch */ 
        if (prefetch){
                
                for (int i=0; i<associativity; i++){
                        //cache_block_obl[i].dirty =false;
                /*Check if  Block B + 1 is in cache */ 
                        if (cache_block_obl[i].tag != tag){
                                
                                cache_block_obl[i].obl_tag = 1; 
                                lru_replacement_policy(idx, tag, associativity, loadstore, cache_block_obl, operation_result_cache_obl, debug);
                               
                                cache_block_obl[i].dirty =false; 
                                if (loadstore){
                                        operation_result_cache_obl -> miss_hit = MISS_STORE;
                                } else {
                                        operation_result_cache_obl -> miss_hit = MISS_LOAD; 
                                }
                                //}

                        } else {
                                hit_obl = true; 
                                 if (loadstore){
                                        operation_result_cache_obl -> miss_hit = HIT_STORE;
                                } else {
                                        operation_result_cache_obl -> miss_hit = HIT_LOAD; 
                                }
                                cache_block_obl[i].dirty =false; 
                        }
                         /* If Block B + 1 is in cache, no action is needed */  
                         //cache_block_obl[i].dirty =false;
                }
                if (hit_flag){
                        if (loadstore){
                                operation_result_cache_block -> miss_hit = HIT_STORE;
                        } else {
                                operation_result_cache_block -> miss_hit = HIT_LOAD; 
                        }
                }
                if (hit_obl){
                        if (loadstore){
                                operation_result_cache_obl -> miss_hit = HIT_STORE;
                        } else {
                                operation_result_cache_obl -> miss_hit = HIT_LOAD; 
                        }
                }
        }//prefetch

        /*Update dirty bits for obl block */ 
        for (int i=0; i<associativity; i++){
                cache_block_obl[i].dirty =false;
        }
    
   return OK;
}

