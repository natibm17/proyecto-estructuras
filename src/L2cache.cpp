/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;


int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug)
{
	 /* Check parameters */
	int param_check_l1 = params_check(l1_l2_info->l1_idx, l1_l2_info->l1_tag, l1_l2_info->l1_assoc);
	int param_check_l2 = params_check(l1_l2_info->l2_idx, l1_l2_info->l2_tag, l1_l2_info->l2_assoc);
	if (param_check_l1!=OK || param_check_l2!=OK){
		return PARAM;
	}
	bool L1_HIT = false;
	bool L2_HIT = false;
	bool lru_found = false;
	for (int way = 0; way < l1_l2_info->l1_assoc; way++)
	{
	/************************************** L1 HIT L2 HIT **************************************/
		if( (l1_l2_info->l1_tag == l1_cache_blocks[way].tag) && l1_cache_blocks[way].valid)
		{
			L1_HIT = true;
			L2_HIT = true;
			/* Update rp values*/
			for(int way_up = 0; way_up < l1_l2_info->l1_assoc; way_up++)
			{
				if (l1_cache_blocks[way_up].rp_value > l1_cache_blocks[way].rp_value)
				{
					if (l2_cache_blocks[way_up].rp_value > 0)
					{
						l2_cache_blocks[way_up].rp_value--;
					}
				}
			}
			/* MRU */
			l1_cache_blocks[way].rp_value = l1_l2_info->l1_assoc-1;

			/* HIT L2 */
			for(int way_l2 = 0; way_l2 < l1_l2_info->l2_assoc; way_l2++)
			{
				if(l1_l2_info->l2_tag == l2_cache_blocks[way_l2].tag && l2_cache_blocks[way_l2].valid)
				{
					/* Update rp values*/
					for(int way_up = 0; way_up < l1_l2_info->l2_assoc; way_up++)
					{
						if (l2_cache_blocks[way_up].rp_value > l2_cache_blocks[way_l2].rp_value)
						{
							if (l2_cache_blocks[way_up].rp_value > 0)
							{
								l2_cache_blocks[way_up].rp_value--;
							}
						}
					}
					/* MRU */
					l2_cache_blocks[way_l2].rp_value = l1_l2_info->l2_assoc-1;
					/* Save results */
					if (loadstore)  /* Store */
					{
						l1_result->miss_hit = HIT_STORE;
						l2_result->miss_hit = HIT_STORE;
						l2_cache_blocks[way].dirty = true;
					}
					else{  /* Load */

						l1_result->miss_hit = HIT_LOAD;
						l2_result->miss_hit = HIT_LOAD;
					}
					/* There is no eviction */
					l1_result->dirty_eviction = false;
					l2_result->dirty_eviction = false;
					l1_result->evicted_address = 0;
					l2_result->evicted_address = 0;

					break;
				}
			}
			break;
		}
	}

	/************************************* L1 MISS L2 HIT **************************************/
		/* L1 MISS */
	if (!L1_HIT)
	{
		for (int way_l2 = 0; way_l2 < l1_l2_info->l2_assoc; way_l2++)
		{
			/* L2 HIT */
			if (l1_l2_info->l2_tag == l2_cache_blocks[way_l2].tag)
			{
				L2_HIT = true;
				/* Update rp values*/
				for(int way_up = 0; way_up < l1_l2_info->l2_assoc; way_up++)
				{
					if (l2_cache_blocks[way_up].rp_value > l2_cache_blocks[way_l2].rp_value)
					{
						if (l2_cache_blocks[way_up].rp_value > 0)
						{
							l2_cache_blocks[way_up].rp_value--;
						}
					}
				}
				/* MRU */
				l2_cache_blocks[way_l2].rp_value = l1_l2_info->l2_assoc-1;
				/* Save results */
				if (loadstore) /* Store */
				{
					l2_cache_blocks[way_l2].dirty = true;
					l1_result->miss_hit = MISS_STORE;
					l2_result->miss_hit = HIT_STORE;
				}
				else{ /* Load */
					l1_result->miss_hit = MISS_LOAD;
					l2_result->miss_hit = HIT_LOAD;
				}
				/* There is no eviction */
				l1_result->dirty_eviction = false;
				l2_result->dirty_eviction = false;
				l1_result->evicted_address = 0;
				l2_result->evicted_address = 0;
				break;
			}
		}
	}

	/************************************** L1 MISS L2 MISS **************************************/
	if (!L2_HIT)
	{
		/* Find LRU */
		for (int way_l2 = 0; way_l2 < l1_l2_info->l2_assoc; way_l2++)
		{
			if(l2_cache_blocks[way_l2].rp_value == 0)
			{
				/* Update rp values*/
				for(int way_up = 0; way_up < l1_l2_info->l2_assoc; way_up++)
				{
					if (l2_cache_blocks[way_up].rp_value > 0)
					{
						l2_cache_blocks[way_up].rp_value--;
					}
				}
				// Update
				l2_result->evicted_address = l2_cache_blocks[way_l2].tag;
				l2_cache_blocks[way_l2].rp_value = l1_l2_info->l2_assoc-1;
				l2_cache_blocks[way_l2].tag = l1_l2_info->l2_tag;
				l2_cache_blocks[way_l2].valid = true;
				if (l2_cache_blocks[way_l2].dirty)
				{
					l2_result->dirty_eviction = true;
				}
				else
				{
					l2_result->dirty_eviction = false;
				}
				if (loadstore) /* store */
				{
					l2_result->miss_hit = MISS_STORE;
					l2_cache_blocks[way_l2].dirty = true;
				}
				else /* load */
				{
					l2_result->miss_hit = MISS_LOAD;
					l2_cache_blocks[way_l2].dirty = false;
				}
				break;
			}
		}
	}

	if (!L1_HIT)
	{
		for (int way_l1 = 0; way_l1 < l1_l2_info->l1_assoc; way_l1++)
		{
			if(l1_cache_blocks[way_l1].rp_value == 0)
			{
				/* Update rp values*/
				for(int way_up = 0; way_up < l1_l2_info->l1_assoc; way_up++)
				{
					if (l1_cache_blocks[way_up].rp_value > 0)
					{
						l1_cache_blocks[way_up].rp_value--;
					}
				}
				/* MRU */
				l1_cache_blocks[way_l1].rp_value = l1_l2_info->l1_assoc-1;
				l1_result->evicted_address = l1_cache_blocks[way_l1].tag;
				l1_cache_blocks[way_l1].tag = l1_l2_info->l1_tag;
				l1_cache_blocks[way_l1].valid = true;
				l1_result->dirty_eviction = false;
				if (loadstore)  /* store */
				{
					l1_result->miss_hit = MISS_STORE;
				}
				else  /* load */
				{
					l1_result->miss_hit = MISS_LOAD;
				}
				break;
			}
		}
	}
	return OK;
}
