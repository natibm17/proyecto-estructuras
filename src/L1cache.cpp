/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/debug_utilities.h"
#include "../include/L1cache.h"

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int params_check(int idx, int tag, int associativity){
  int ok_param = OK;
  int assoc2 = associativity;

  if (idx >= 0 && tag >= 0 && associativity > 0) {
    //checks if associativity is a power of 2
    while(assoc2%2==0 && assoc2 > 1){
      assoc2 /= 2;
    }
    if (associativity%2!=0){
      ok_param = PARAM;
    }
    if (assoc2==1){
      ok_param = OK;
    }else {
      ok_param = PARAM;
    }
  } else {
    ok_param = PARAM;
  }
  return ok_param;
}
int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{

   /* Offset */
   field_size->offset = log2(cache_params.block_size);
   /* Index */
   field_size->idx = log2(cache_params.size*1024)- log2(cache_params.asociativity) - (field_size->offset);
   /* Tag */
   field_size->tag = ADDRSIZE - (field_size->idx) - (field_size->offset);
   return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
   /* Tag */
   long temp = address >> (field_size.idx + field_size.offset);
   *tag = (int)temp;

   /* Index */
   temp = address << field_size.tag;
   temp = temp & 0xffffffff;
   temp = temp >> (field_size.tag + field_size.offset);
   *idx = (int)temp;
}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  int param_check = params_check(idx, tag, associativity);
  if (param_check!=OK){
    return PARAM;
  }
  /* rrpv DEFINITION */
  int m;
  bool hit_flag = false;
  bool full = true;
  int empty_space = 0;
  int replacement =0;
  int victim =0;
  bool update = false;
  if (associativity<=2){
    m = 1;
  } else if (associativity>2){
    m=2;
  }
  int rrpv = pow(2,m)-2; //re reference prediction value

  for (int way = 0; way < associativity; way++)
  {
    /* HIT */
    if (cache_blocks[way].valid && (cache_blocks[way].tag == tag) )
    {
      /*Update RP value to near inmediate */
      cache_blocks[way].rp_value = 0;

      /* There is no eviction */
      result-> dirty_eviction = false;
      result->evicted_address =0;

      /* Set hit flag to true */
      hit_flag = true;
      /* Load */
      if (loadstore == false)
      {
        result->miss_hit = HIT_LOAD;
      }
      /* Store */
      else if (loadstore == true)
      {
        result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
      }
      break;
    }
    else if (cache_blocks[way].valid == false){
      full = false;
      replacement = way;
      break;
    }
  }
  if (hit_flag){
    return OK;
  }
  /* MISS */
  else
  {
    if (loadstore){
      result->miss_hit = MISS_STORE;
    } else {
      result -> miss_hit = MISS_LOAD;
    }
    /* Cache Full */
    if (full){
      update = true;
      for (int way = 0; way <associativity; way++){
        /*Eviction */
        if (cache_blocks[way].rp_value == rrpv+1){
          /* Select victim position */
          victim = way;
          update = false;
          break;
        }
      }//for

      bool complete = false;

      /*Update rp_values*/
      if (update){
        while (!complete){
          for (int i=0; i<associativity; i++){
            cache_blocks[i].rp_value++;
          }
          for (int i=0; i<associativity; i++){
            if (cache_blocks[i].rp_value== rrpv+1){
              victim =i;
              complete = true;
              break;
            }
          }
        }
      }
      /* Eviction */
      if (cache_blocks[victim].dirty){
        result->dirty_eviction = true;
      } else {
        result -> dirty_eviction = false;
      }
      result-> evicted_address = cache_blocks[victim].tag;

      /*Set Dirty bit */
      if (!loadstore){
        cache_blocks[victim].dirty = false;
      } else {
        cache_blocks[victim].dirty = true;
      }
      cache_blocks[victim].tag = tag;
      cache_blocks[victim].valid = true;
      cache_blocks[victim].rp_value = rrpv;
      return OK;
    }
    else {
     /*No eviction */
      result -> evicted_address =0;
      result-> dirty_eviction = false;

      /*New value with rp_value */
      cache_blocks[replacement].tag = tag;
      cache_blocks[replacement].rp_value = rrpv;
      cache_blocks[replacement].valid = true;

      /*Set Dirty bit */
      if (!loadstore){
        cache_blocks[replacement].dirty = false;
      } else {
        cache_blocks[replacement].dirty = true;
      }
      return OK;
    }
  } //miss
  return ERROR;
}



int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

  bool full = true;
  bool hit_flag = false;
  int lru_val = 0;
  int mru_val = associativity - 1;
  int hit_way = -1;
  int free_way = -1;
  int victim = -1;
  int param_check = params_check(idx, tag, associativity);

  if (param_check!=OK){
    return PARAM;
  }

  for (int way = 0; way < associativity; way++){
    //Hit
    if (cache_blocks[way].valid && cache_blocks[way].tag == tag){
      hit_flag = true;
      hit_way = way;
    }
    //Space check
    if (cache_blocks[way].valid == false) {
      full = false;
      free_way = way;

    }
  }
  //check hit status
  if (hit_flag){
    //Store
    if (loadstore){
      result->miss_hit = HIT_STORE;
    //Load
    }else{
      result->miss_hit = HIT_LOAD;
    }
    result->evicted_address = 0;
    result->dirty_eviction = false;

    for (int i = 0; i < associativity; i++){
      if (cache_blocks[i].rp_value>cache_blocks[hit_way].rp_value){
        if (cache_blocks[i].rp_value!=lru_val){
          cache_blocks[i].rp_value = cache_blocks[i].rp_value - 1;//lru substraccion
        }
      }
    }
    cache_blocks[hit_way].rp_value = mru_val;//hit gets max value
    if(loadstore){
      cache_blocks[hit_way].dirty = true;
    }
  //Miss
  }else{
    //Store
    if (loadstore){
      result->miss_hit = MISS_STORE;
    //Load
    }else{
      result->miss_hit = MISS_LOAD;
    }
    //Free Space
    if (full == false){
      result->dirty_eviction = false;
      result->evicted_address = 0;
      for (int i = 0; i < associativity; i++){
        if (cache_blocks[i].valid){
          if (cache_blocks[i].rp_value != lru_val){
            cache_blocks[i].rp_value = cache_blocks[i].rp_value - 1;//lru substraction
          }
        }
      }
      //check dirty
      if (loadstore){
        cache_blocks[free_way].dirty = true;
      }else{
        cache_blocks[free_way].dirty = false;
      }
      cache_blocks[free_way].rp_value = mru_val;//insertion gets max value
      cache_blocks[free_way].tag = tag;
      cache_blocks[free_way].valid = true;

    //full cache
    }else{
      for (int i = 0; i < associativity; i++){
        if (cache_blocks[i].rp_value == lru_val){
          victim = i;//fids victim
        }else{
          cache_blocks[i].rp_value = cache_blocks[i].rp_value -1;//lru substracion
        }
      }
      result->evicted_address = cache_blocks[victim].tag;
      if (cache_blocks[victim].dirty){
        result->dirty_eviction = true;
      }else{
        result->dirty_eviction = false;
      }
      //check dirty
      if (loadstore){
        cache_blocks[victim].dirty = true;
      }else{
        cache_blocks[victim].dirty = false;
      }
      cache_blocks[victim].rp_value = mru_val;//insertion gets max value
      cache_blocks[victim].valid = true;
      cache_blocks[victim].tag = tag;


    }
  }
  return OK;
}
int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{
  bool nru = false;
  bool hit = false;

  /* Check parameters */
  int param_check = params_check(idx, tag, associativity);
  if (param_check!=OK){
    return PARAM;
  }


  for (int way = 0; way < associativity; way++)
  {
    /* HIT*/
    if (cache_blocks[way].valid && (cache_blocks[way].tag == tag) )
    {
      hit = true;
      /* Load */
      if (loadstore == false)
      {
        operation_result->miss_hit = HIT_LOAD;
      }
      /* Store */
      else if (loadstore == true)
      {
        operation_result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
      }
      /* There is no eviction */
      operation_result->dirty_eviction = false;
      operation_result->evicted_address = 0;

      cache_blocks[way].rp_value = 0;
      break;
    }
  }
  /* Miss */
  if (!hit)
  {
    /* Find NRU */
    for (int way = 0; way < associativity; way++)
    {
      if (cache_blocks[way].rp_value == 1)
      {
        nru = true;
      }
    }
    /* NRU not found*/
    if (!nru)
    {
      for (int way = 0; way < associativity; way++)
      {
        cache_blocks[way].rp_value = 1;
      }
    }
    /* Eviction*/
    for (int way = 0; way < associativity; way++)
    {
      /* Find NRU*/
      if (cache_blocks[way].rp_value == 1)
      {
        /* Write back*/
        if(cache_blocks[way].dirty )
        {
          operation_result->dirty_eviction = true;
        }
        else
        {
          operation_result->dirty_eviction = false;
        }
        /* Update results and entry data*/
        operation_result->evicted_address = cache_blocks[way].tag;
        cache_blocks[way].tag = tag;
        cache_blocks[way].rp_value = 0;
        if (loadstore == true)
        {
          cache_blocks[way].dirty = true;
          operation_result->miss_hit = MISS_STORE;
        }
        else if (loadstore == false)
        {
          cache_blocks[way].dirty = false;
          operation_result->miss_hit = MISS_LOAD;
        }
        cache_blocks[way].valid = 1;
        break;
      }
    }
  }
  return OK;
}

entry **create_cache(int rp, int ways, int idx_size)
{
  /* Sets */
  int sets = pow(2, idx_size);
  /* Create cache */
  entry **cache = new entry*[sets];
  for (int i = 0; i < sets; i++) {
    cache[i] = new entry[ways];
  }
  if (rp == LRU)
  {
    for (int i = 0; i < sets; i++) {
      for (int j = 0; j < ways; j++) {
        cache[i][j].valid = false;
        cache[i][j].dirty = false;
        cache[i][j].tag = 0;
        cache[i][j].rp_value = 3;//check
      }
    }
  }
  else if (rp == NRU)
  {
    for (int i = 0; i < sets; i++) {
      for (int j = 0; j < ways; j++) {
        cache[i][j].valid = false;
        cache[i][j].dirty = false;
        cache[i][j].tag = 0;
        cache[i][j].rp_value = 1;
      }
    }
  }
  else if (rp == RRIP)
  {
    for (int i = 0; i < sets; i++) {
      for (int j = 0; j < ways; j++) {
        cache[i][j].valid = false;
        cache[i][j].dirty = false;
        cache[i][j].tag = 0;
        cache[i][j].rp_value = 3;
      }
    }

  }
  return cache;
}
